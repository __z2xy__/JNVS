package com.control.api;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONObject;
import com.control.db.entity.CameraEntity;

@Component
public class MediaApi {
	@Autowired
	RestTemplate restTemplate;
	@Value("${media.server-ip}")
	String mediaIp;
	@Value("${media.api-port}")
	Integer mediaApiPort;
	@Value("${media.secret}")
	String mediaSecret;
	/***
	 * 开始录制
	 * @return
	 */
	public Boolean startRecord(CameraEntity cameraEntity) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("stream", cameraEntity.getLiveKey());
		if(cameraEntity.getType() == 1) {
			params.put("app", "rtp");
		} else if(cameraEntity.getType() == 3) {
			params.put("app", "live");
		} else {
			return false;
		}
		params.put("secret", mediaSecret);
		String result = restTemplate.getForObject("http://"+mediaIp+":"+mediaApiPort+"/index/api/startRecord?secret={secret}&type=1&vhost=__defaultVhost__&wait_for_record=1&continue_record=1&app={app}&stream={stream}", String.class,params);
		JSONObject jsonObj = JSONObject.parseObject(result);
		Integer code = jsonObj.getInteger("code");
		if(code == 0) {
			return true;
		} else {
			return false;
		}
	}
	/***
	 * 结束录制
	 * @param key
	 * @return
	 */
	public Boolean stopRecord(CameraEntity cameraEntity) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("stream", cameraEntity.getLiveKey());
		if(cameraEntity.getType() == 1) {
			params.put("app", "rtp");
		} else if(cameraEntity.getType() == 3) {
			params.put("app", "live");
		} else {
			return false;
		}
		params.put("secret", mediaSecret);
		String result = restTemplate.getForObject("http://"+mediaIp+":"+mediaApiPort+"/index/api/stopRecord?secret={secret}&type=1&vhost=__defaultVhost__&app={app}&stream={stream}", String.class,params);
		JSONObject jsonObj = JSONObject.parseObject(result);
		Integer code = jsonObj.getInteger("code");
		if(code == 0) {
			return true;
		} else {
			return false;
		}
	}
	/***
	 * 动态添加rtsp/rtmp拉流代理(只支持H264/H265/aac负载)
	 * @return
	 */
	public Boolean addStreamProxy(String key,String url) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("stream",key);
		params.put("app", "live");
		params.put("secret", mediaSecret);
		params.put("url", url);
		String result = restTemplate.getForObject("http://"+mediaIp+":"+mediaApiPort+"/index/api/addStreamProxy?secret={secret}&vhost=__defaultVhost__&app={app}&stream={stream}&url={url}&enable_rtsp=1&enable_rtmp=1&rtp_type=0", String.class,params);
		JSONObject jsonObj = JSONObject.parseObject(result);
		Integer code = jsonObj.getInteger("code");
		if(code == 0) {
			return true;
		} else {
			return false;
		}
	}
	/***
	 * 关闭拉流代理
	 * @return
	 */
	public Boolean delStreamProxy(String key) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("key", key);
		params.put("secret", mediaSecret);
		String result = restTemplate.getForObject("http://"+mediaIp+":"+mediaApiPort+"/index/api/delStreamProxy?secret={secret}&key={key}", String.class,params);
		JSONObject jsonObj = JSONObject.parseObject(result);
		Integer code = jsonObj.getInteger("code");
		if(code == 0) {
			if(jsonObj.getJSONObject("data").getBoolean("flag")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
}
