package com.control.api;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONObject;

@Component
public class OnvifApi {
	@Autowired
	RestTemplate restTemplate;
	@Value("${onvif.server-ip}")
	String onvifServerIp;
	@Value("${onvif.server-http-port}")
	String onvifServerHttpPort;
	
	public String live(String deviceId,String channelId) {
		String result = restTemplate.getForObject("http://"+onvifServerIp+":"+onvifServerHttpPort+"/onvif/live/"+deviceId+"_"+channelId, String.class);
		JSONObject jsonObj = JSONObject.parseObject(result);
		Integer code = jsonObj.getInteger("code");
		if(code == 200) {
			return jsonObj.getString("data");
		} else {
			return null;
		}
	}
	public Boolean ptz(String deviceId,String channelId,int leftRight, int upDown, int inOut){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("leftRight", leftRight);
		params.put("upDown", upDown);
		params.put("inOut", inOut);
		String result = restTemplate.getForObject("http://"+onvifServerIp+":"+onvifServerHttpPort+"/onvif/ptz/"+deviceId+"_"+channelId+"?leftRight={leftRight}&upDown={upDown}&inOut={inOut}", String.class,params);
		JSONObject jsonObj = JSONObject.parseObject(result);
		Integer code = jsonObj.getInteger("code");
		if(code == 200) {
			return true;
		} else {
			return false;
		}
	}
}
