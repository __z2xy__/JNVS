package com.control.bean.gb28181;

import java.util.Map;


public class Gb28181DeviceBean {

	/**
	 * 设备Id
	 */
	private String deviceId;

	/**
	 * 传输协议
	 * UDP/TCP
	 */
	private String transport;
	private String ip;
	private int port;
	private String address;

	/**
	 * wan地址
	 */
	//private Host host;
	
	/**
	 * 通道列表
	 */
	private Map<String,Gb28181ChannelBean> channelMap;
	
	
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getTransport() {
		return transport;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Map<String, Gb28181ChannelBean> getChannelMap() {
		return channelMap;
	}

	public void setChannelMap(Map<String, Gb28181ChannelBean> channelMap) {
		this.channelMap = channelMap;
	}

}
