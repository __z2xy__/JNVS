package com.control.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "t_camera")
@Data
public class CameraEntity {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	/***
	 * 类型 
	 *1-gb28181
	 *2-onvif
	 *3-rtsp
	 */
	@Column(name="type")
	Integer type;
	/***
	 * 设备id
	 */
	@Column(name="device_id")
	String deviceId;
	/***
	 * 通道id
	 */
	@Column(name="channel_id")
	String channelId;
	
	/***
	 * 1-在线
	 * 0-离线
	 */
	@Column(name="status")
	Integer status;
	
	/***
	 * 录制状态
	 * 0-未录制
	 * 1-录制中
	 */
	@Column(name="record_status")
	Integer recordStatus;
	
	/***
	 * 直播状态
	 * 1-直播中
	 * 0-未直播
	 */
	@Column(name="live_status")
	Integer liveStatus;
	/***
	 * 直播key
	 */
	@Column(name="live_key")
	String liveKey;
	/***
	 * 品牌id
	 */
	@Column(name="brand_id")
	Long brandId;
	/***
	 * ip地址
	 */
	@Column(name="ip")
	String ip;
	/***
	 * rtsp 通道
	 */
	@Column(name="rtsp_channel")
	String rtspChannel;
	/***
	 * rtsp port
	 */
	@Column(name="rtsp_port")
	Integer rtspPort;
	/***
	 * rtsp使用
	 */
	@Column(name="username")
	String username;
	/***
	 * rtsp使用
	 */
	@Column(name="password")
	String password;
	
	@Column(name="onvif_username")
	String onvifUsername;
	@Column(name="onvif_password")
	String onvifPassword;
	@Column(name="onvif_port")
	Integer onvifPort;
	
	
	
}
